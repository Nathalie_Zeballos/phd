This data repository correspond to the article : XXX
Authors : XXX
The first author was responsible for collecting data and writing the code. 

This study reports on our extensive experimental investigation on whether, and how, environmentally induced death may paradoxically be correlated to higher fitness. 
We tracked the population dynamics of two closely related strains of the halotolerant microalga Dunaliella salina, following transfers across salinities. 

This repositeroy contains 
INPUT repository:
	-data_predictors.csv : A csv file needed to predict from glm for the main script "Analysis_Figures.rmd"
	7 data files, each one corresponding to a specific experiment:
	-exp1-5_filtrate.csv : This data file contains dynamics measures from the experiment 1 (salinity change) and experiment 5 (cultures on population filtrates). 
	-*exp2_inhibitor.csv : This data file contains dynamics measures from the experiment 2 (PCD inhibitor). 
	-exp3_age_density.csv : This data file contains dynamics measures from the experiment 3 (initial density and population growth phase)
	-exp4_ressources.csv : This data file contains dynamics measures from the experiment 4 (nutrient limitation). 
	-exp6_light.csv : This data file contains dynamics measures from the experiment 6 (light intensity).
	-exp7retransfer.csv : This data file contains dynamics measures from the experiment 7 (successive osmotic shocks).  
	-*preliminaryexp_inhibitor.csv : This data file is only used for a supplementary figure. 
All input files (except *) are called for the main script. Data files marked * are called for the "analysis_inhibitor.rmd" script. 

OUTPUT repository: 
	MAIN_FIG: The 5 figures in the main text. 
	RESULT_DATA: 
		-predictionF_declin_inhibitor.csv : Predicted values of Decline and rebound from the inhibitor assay. 
		-simulationLM_rebound_decline_REFAIT_12sept :result of the 10 000 simulated LM between decline and rebound. 
		-simulationLM_rebound_decline_final_REFAIT_12sept : result of the 10 000 simulated LM between decline and rebound, adding more variance. 
		-tables_gml interactions.csv : an excel file containing all the glm outputs to shape the tables.  
	SUPP_FIG: The 9 figures for the supplementary material. 

-"Analysis_Figures.rmd": the main Rmarkdown script, containing code for analysis, tables and figures (major and supplementary material). 
-"analysis_inhibitor.rmd" : a Rmarkdown script only for the analysis of the inhibitor assay.
	/!\ Run first if you want to create by yourself the predicted values for decline and r0 (=predictionF_declin_inhibitor.csv ), which will be needed for the Fig5. 
	The other outputs of this secondary script are Fig 2, Fig S3 and Fig S4. 