## Title of Zenodo repository: Fitness of cell death

This code repository corresponds to the article "Acceptable loss: Fitness consequences of salinity-induced cell death in a halotolerant microalga"
Authors : Nathalie Zeballos, Daphné Grulois, Christelle Leung and Luis-Miguel Chevin
Journal: The American Naturalist
Contact: zeballosnathalie@gmail.com
The first author was responsible for collecting data and writing code. 

This study reports on our extensive experimental investigation on whether, and how, environmentally-induced cell death may paradoxically be correlated to higher fitness. 
We tracked the population dynamics of two closely related strains of the halotolerant microalga Dunaliella salina, following transfers across salinities. 

## Description of the workflow
Data repository can be found on Dryad.
Raw data are .fcs files opened in the software Incyte (version 3.2), from where we extracted the population densities. 
Whole-well fluorescence spectrometer raw data (.RUC files) were opened in the software Mars (version 3.32).
We built the inputs files based on these raw data. 
All statistical analyses were performed on Rstudio (R version 3.6) using MASS version 7.3.53, tidyverse 1.3.1 and lubridate 1.8.0 packages. 
Workflow instruction: Most of analyses and figures are performed in Analyss_Figures.rmd script, you can direclty run this script if you have the file 'predictionF_declin_inhibitor.csv' . 
/!\ If you want to create by yourself the predicted values for decline and r0 of the inhibitor assay (=predictionF_declin_inhibitor.csv ), which will be needed for the Fig5, Run first "analysis_inhibitor.rmd".

## This repositeroy contains:
Script files
-"Analysis_Figures.rmd": the main Rmarkdown script, containing code for analysis, tables and figures (major and supplementary material). 
-"analysis_inhibitor.rmd" : a Rmarkdown script only for the analysis of the inhibitor assay.
	/!\ Run first if you want to create by yourself the predicted values for decline and r0 (=predictionF_declin_inhibitor.csv ), which will be needed for the Fig5. 
	The other outputs of this secondary script are Fig 2, Fig S3 and Fig S4. 

SUPPLEMENTARY INFORMATION: 
- Author-supplied.pdf : A pdf file including the supplemntary tables and figures. 
