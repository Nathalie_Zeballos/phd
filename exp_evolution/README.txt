This repositeroy contains data and analysis concerning the article XXX, on the long-term competition btween 2 strains of Dunaliella salina.
These long-term populations (monocultures et cocultures) alternated intermediate salinity ([NaCl]= 2.4M) and high salinity ([NaCl]= 4M) for a cycle, over 10 cycles.
We only kept the long-term cocultures 16 cycles more.
2 temporal variation treatment, whereby a cycle was composed by 7 days at intermediate salinity followed by 7 days at high salinity; while the 2nd treatment= 4d-10d.
The raw datas are cytometer files (.fcs) from where we can export a csv file where 1 observation correspond to 1 "event" (one
particule), these csv have been used to estimate frequency of each strain.
From the software Incyte we can also export the population densities calculated per well on which we performed the population dynamics analyses. 


DATA:
data_env1_10.csv & .xlsx : populations densities for the transfers n°1 to 10 included, i.e. 5 salinity cycles, with daily measures for the high salinity environments (even numbers). Only 2 measures for the intermediate salinity envionment : first and final day. 
data_env11.csv & .xlsx : populations densities for the transfers n°11 to 16 included, i.e. 8 supplementary salinity cycles with only 3 measures per high salinity enviroments, except for the 2 last transfer (24 &26) with daily measures. 
data_NZ: don't need it because redundnat with data_env1_10 ? 
strains_NZ.csv & .xlsx : Information for each flask used in the long-term experiment. 

OUTPUT:
PLOT:

fast_analysis_evolNZ.RMD:a Rmardown file for analyse popuation dynamics: decline, growth rate over the salinity cycles. We also estimatee the frequency of strain based on the decline values.
This file neeed as input the datasets contained in the repository DATA. The produced files (complete dataset, glms) are registered in OUTPUT, while the figures go to the PLOT repository.  