This repositeroy contains the second step of analysis concerning the article XXX, on the long-term competition btween 2 strains of Dunaliella salina.
It contains the estimation of each strain frequency in the mixtures, based on the raw data of cytometer counts.
FCT: fct_freq.R is the code to estimates frequency of each strain in mixtures based on mean values of the monocultures.
INPUT:
	CALIBRATION: files needed for the checking script calibration_freq.Rmd

OUTPUT:
PLOT:

R scripts:
calibration_freq.Rmd : script to validate our estimation method. Outputs: Fig S1 and S2
freq_moydef.R : Script original par LM Chevin, used as example. 
-> at the end remove if dont need
main_mixtures.R : script with code to estimate frequencies of the first 10 transferts (long-term monocultures, control and long-term mixtures)
mixtures.Rmd : script with code to estimate frequencies of the last transferts n°11 to 26 (only long-term mixtures)
--> check and merge if possible the scripts main_mixtures and mixtures.Rmd