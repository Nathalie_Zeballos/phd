This repositery "1_densities-analysis" contains data and analysis concerning the article XXX, on the long-term competition between 2 strains of Dunaliella salina.
These long-term populations (monocultures et mixtures) alternated intermediate salinity ([NaCl]= 2.4M) and high salinity ([NaCl]= 4M) for a cycle, over 10 cycles.
We further kept the long-term mixtures 16 cycles more.
2 temporal variation treatment, whereby a cycle was composed by 7 days at intermediate salinity followed by 7 days at high salinity 7d-7d; while the 2nd treatment= 4d-10d.
These analyses are based on populations densities, the raw data of cytometer counts are used in another repositery to estimate frequency of each strain. 

INPUT:
data_transfer1_10.csv & .xlsx : populations densities for the transfers n°1 to 10 included, i.e. 5 salinity cycles, with daily measures for the high salinity environments (even numbers).
Only 2 measures for the intermediate salinity envionment : first and final day. 
data_transfer11_26.csv & .xlsx : populations densities for the transfers n°11 to 26 included, i.e. 8 supplementary salinity cycles with only 3 measures per high salinity environments (T0, T1 and Tfinal day), 
except for the 2 last transfer (24 &26) with daily measures. 
data_NZ: don't need it because redundant with data_env1_10 ? 
strains_NZ.csv & .xlsx : Information for each flask used in the long-term experiment. 

OUTPUT: 
transfer1_10_completedT0.csv : Tidy and completed data for the transfers n°1 to 10.
transfer1_26_completedT0.csv : Tidy and completed data for the transfers n°1 to 26 (all cycles). 
transfer11_26_completedT0.csv : Tidy and completed data for the transfers n°11 to 26. 
PLOT:

R scripts:
fast_analysis_evolNZ.RMD:a Rmarkdown file to tidy and complete data sets. 
This file neeed as input the datasets contained in the repository INPUT. The produced files (complete dataset) are registered in OUTPUT, while the figures go to the PLOT repository.
analysis_GLM.RMD: a Rmarkdown script to analyse popuation dynamics: decline, growth rate over the salinity cycles. [We also estimatee the frequency of strain based on the decline values. NOT YET]