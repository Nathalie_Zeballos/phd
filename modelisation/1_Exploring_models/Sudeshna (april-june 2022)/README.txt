List of files

1. Equations with alpha
Mathematica script. Includes analytical solutions for bad cell proportion in strain A monoculture (p), proportion of strain A in coculture (q), cell density of monoculture A, cell density of monoculture C, and total cell density in coculture.
Equations written in terms of alpha.

2. Equations with k
Mathematica script. Includes analytical solutions and simulations for all parameters as mentioned above.
Equations written in terms of carrying capacity, k.
Simulations based on parameter estimate from model fitting in R.
Contains a summary of all solutions (in terms of k).

3. Final Report
Submitted report. Both word and pdf format included.

4. Final Presentation
Presentation of final thesis defense.

5. Plots
Contains all figures included in the report. Figures are named with corresponsponding figure number from report.

6. Simulation(with data).R
R script of simulation and model fitting. Simulations not used in report.
Parameters estimated using stress phase data for strain A monoculture and benign phase (acclimatisation) data for strain C monoculture.
Includes simulation of coculture with parameter values estimated from model fitting (not included in report).

7. transfer1_10_completedT0.csv
Data from experiments (received from Nathalie). Includes cell density measure at time zero (initial density at transfer). Used for parameter estimation of strain A monoculture.

8. data_accli.csv
Data from experiments (received from Nathalie). Includes cell density measures during acclimatisation (benign) phase. Used for parameter estimation of strain C monoculture.